import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class MainLayout extends StatefulWidget {
  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return /*SafeArea(
      child:*/
        Stack(
      fit: StackFit.expand,
      overflow: Overflow.clip,
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Color(0xFFFFE0B2),
        ),
        Positioned(
          top: -screenHeight * 0.015,
          right: screenWidth * 0.05,
          height: (screenHeight * 0.5),
          width: (screenWidth * 0.5),
          child: Container(
            decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Colors.red, width: screenWidth * 0.005)),
          ),
        ),
        Positioned(
          right: -screenWidth * 0.33,
          top: -screenHeight * 0.2,
          height: (screenHeight * 0.7),
          width: (screenWidth * 0.7),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            ),
          ),
        ),
        Positioned(
          right: -screenWidth * 0.12,
          top: screenHeight * 0.12,
          height: (screenHeight * 0.25),
          width: (MediaQuery.of(context).size.width * 0.25),
          child: Container(
            decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Colors.white, width: screenWidth * 0.005)),
          ),
        ),
        Positioned(
          left: screenWidth * 0.06,
          bottom: -screenHeight * 0.15,
          height: (screenHeight * 0.3),
          width: (screenWidth * 0.3),
          child: Container(
            decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Colors.red, width: screenWidth * 0.005)),
          ),
        ),
        Positioned(
          left: -screenWidth * 0.1,
          bottom: -screenHeight * 0.12,
          height: (screenHeight * 0.33),
          width: (screenWidth * 0.33),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            ),
          ),
        ),
        Positioned(
          left: -screenWidth * 0.075,
          bottom: screenHeight * 0.06,
          height: (screenHeight * 0.15),
          width: (screenWidth * 0.15),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: Colors.white, width: screenWidth * 0.005),
            ),
          ),
        ),
        Positioned(
          top: screenHeight * 0.5,
          left: screenWidth * 0.1,
          width: screenWidth * 0.8,
          height: screenHeight * 0.25,
          child: Column(
            children: [
              Container(
                width: screenWidth * 0.8,
                decoration: BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.black, width: screenWidth * 0.005)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0, top: 10.0),
                  child: Text(
                    'Sample subtitle',
                    style: TextStyle(fontSize: 18),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                width: screenWidth * 0.8,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Sample long Title',
                    style: TextStyle(fontSize: 25),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                width: screenWidth * 0.8,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.',
                    style: TextStyle(fontSize: 11),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: screenHeight * 0.05,
          left: screenWidth * 0.1,
          width: screenWidth * 0.8,
          height: screenHeight * 0.1,
          child: Container(
            decoration: BoxDecoration(
              border: Border.symmetric(
                horizontal: BorderSide(color: Colors.black, width: screenWidth * 0.005),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Custom AppBar'),
                Icon(Icons.menu),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: screenHeight * 0.075,
          height: screenHeight * 0.15,
          width: screenWidth,
          child: CarouselSlider.builder(
            itemCount: 15,
            options: CarouselOptions(
              aspectRatio: 3 / 4,
              viewportFraction: 0.21,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: true,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              scrollDirection: Axis.horizontal,
              enlargeCenterPage: false,
              onPageChanged: (index, reason){
                setState(() {
                  _current = index;
                });
              }
            ),
            itemBuilder: (context, itemIndex) {
              return Container(
                width: screenWidth,
                padding: const EdgeInsets.all(5.0),
                margin: const EdgeInsets.all(5.0),
                decoration: BoxDecoration(color: Colors.black38, borderRadius: BorderRadius.circular(screenWidth * 0.02)),
                child: Icon(
                  Icons.android_rounded,
                  color: _current == itemIndex ? Colors.yellow
                      : Colors.white,
                ),
              );
            },
          ),
        ),
      ],
      // ),
    );
  }
}
